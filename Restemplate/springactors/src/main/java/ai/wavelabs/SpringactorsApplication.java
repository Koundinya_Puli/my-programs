package ai.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringactorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringactorsApplication.class, args);
	}

}
