package ai.wavelabs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActorsController {

	@GetMapping(value = "actors")

	public List<Actors> getActors() {
		List<Actors> li = new ArrayList<Actors>();
		li.add(new Actors("vijay", "rakshitha", "Ravishankar", "vinod", "ramnath"));
		return li;

	}
}
