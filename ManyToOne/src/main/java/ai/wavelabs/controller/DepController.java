package ai.wavelabs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ai.wavelabs.modules.Departement;
import ai.wavelabs.service.DepartementService;

@RestController
public class DepController 
{
	@Autowired
	private DepartementService departementService;
	
	@GetMapping(value="departements")
	public ResponseEntity<List<Departement>> getAllDep()
	{
		List<Departement> li=departementService.getAllDep();
		return ResponseEntity.status(200).body(li);
	}
	
	@PostMapping(value="departements")
	public ResponseEntity<Departement> addDep(@RequestBody Departement dep)
	{
		Departement d=departementService.saveDep(dep);
		return ResponseEntity.status(201).body(d);
		
	}
	 
	
	
	

}
