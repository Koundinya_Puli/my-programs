package ai.wavelabs.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ai.wavelabs.modules.Departement;
@Repository

public interface DepartmentRepository extends JpaRepository<Departement, Integer>
{

}
