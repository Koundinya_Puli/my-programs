package ai.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.wavelabs.modules.Departement;
import ai.wavelabs.repository.DepartmentRepository;

@Service
public class DepartementService 
{
	@Autowired
	private DepartmentRepository departmentRepository;
	
	public Departement saveDep(Departement dep)
	{
		return departmentRepository.save(dep);
	}
	
	public List<Departement> getAllDep()
	{
		List<Departement> li=departmentRepository.findAll();
		return li;
	}
	
	
	
	

}
