package ai.wavelabs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ai.wavelabs.modules.Laptop;
import ai.wavelabs.service.LaptopService;

@RestController
public class LaptopController {
	@Autowired
	private LaptopService laptopservice;

	@GetMapping(value = "laptop", produces = { MediaType.APPLICATION_JSON_VALUE })
	
	public ResponseEntity<List<Laptop>> getAllPersons() {
		List<Laptop> li = laptopservice.getAllPersons();
		return ResponseEntity.status(200).body(li);
	}

	@PostMapping(value = "laptop")
	public ResponseEntity<Laptop> saveLaptops(@RequestBody Laptop laptop) {
		return ResponseEntity.status(201).body(laptopservice.saveLaptops(laptop));

	}

}
