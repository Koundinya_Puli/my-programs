package ai.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.wavelabs.modules.Laptop;
import ai.wavelabs.repository.LaptopPersonsRepository;

@Service
public class LaptopService {
	@Autowired
	private LaptopPersonsRepository laptoppersonsrepository;

	public Laptop saveLaptops(Laptop laptop) {
		return laptoppersonsrepository.save(laptop);
	}

	public List<Laptop> getAllPersons() {
		List<Laptop> li = laptoppersonsrepository.findAll();
		return li;
	}

	public Laptop updateLaptop(Laptop laptop) {
		return laptoppersonsrepository.saveAndFlush(laptop);

	}
}
