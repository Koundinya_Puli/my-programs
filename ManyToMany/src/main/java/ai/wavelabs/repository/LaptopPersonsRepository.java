package ai.wavelabs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import ai.wavelabs.modules.Laptop;
//@Repository
public interface LaptopPersonsRepository extends JpaRepository<Laptop, Integer> {

}
