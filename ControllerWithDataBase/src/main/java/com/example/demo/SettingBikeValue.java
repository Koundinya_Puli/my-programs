package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class SettingBikeValue {
	private Engine engine;
	@Autowired
	@Qualifier("de")
	public void setValue(Engine engine) {
		this.engine = engine;
	}

	public void run() {
		engine.run();
	}

}
