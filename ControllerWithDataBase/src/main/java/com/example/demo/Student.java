package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class Student {
	private int id;
	private String name;
	private int aadharnumber;

	public Student(int id, String name, int aadharnumber) {
		super();
		this.id = id;
		this.name = name;
		this.aadharnumber = aadharnumber;
	}

	public String sayHello() {
		return "Hello";
	}

	@RequestMapping(value = "/hello", method = RequestMethod.POST)
	public ResponseEntity<List<Student>> getStudents() {
		List list = new ArrayList<Student>();
		list.add(new Student(10, "PKS", "1234"));
		list.add(new Student(20, "XYZ", "6789"));
		return ResponseEntity.ok().body(list);

	}

}
