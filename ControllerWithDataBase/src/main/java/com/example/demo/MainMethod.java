package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
@SpringBootApplication
public class MainMethod {

	public static void main(String[] args) {
		ApplicationContext context=SpringApplication.run(MainMethod.class, args);
		SettingBikeValue value=context.getBean(SettingBikeValue.class);
		value.run();

	}

}
