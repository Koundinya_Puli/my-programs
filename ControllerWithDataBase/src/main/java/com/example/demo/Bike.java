package com.example.demo;

import org.springframework.stereotype.Component;

@Component("de")
public class Bike implements Engine {

	@Override
	public void run() {
		System.out.println("Bike is running");
		
	}
	

}
