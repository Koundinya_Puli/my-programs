package com.example.demo;

import org.springframework.stereotype.Component;


@Component("de")
public interface Engine {
	
   public void run();

}
