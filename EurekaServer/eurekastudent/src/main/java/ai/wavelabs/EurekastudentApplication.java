package ai.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekastudentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekastudentApplication.class, args);
	}

}
