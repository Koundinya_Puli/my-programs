package ai.wavelabs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class EurekaschoolApplication {
	@Value("${server.port}")
	private int portNumber;

	public static void main(String[] args) {
		SpringApplication.run(EurekaschoolApplication.class, args);
	}

	@RequestMapping("/port")
	public ResponseEntity<Map> getMap() {
		Map map = new HashMap();
		map.put("port", portNumber);
		return ResponseEntity.status(200).body(map);
	}

}
