package ai.wavelabs.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ai.wavelabs.modules.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>
{

}
