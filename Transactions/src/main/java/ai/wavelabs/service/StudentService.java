package ai.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import ai.wavelabs.modules.Student;
import ai.wavelabs.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	@Transactional
	public Student saveStudent(Student student) {
		printTransactionInfo();
		return studentRepository.save(student);
	}

	private void printTransactionInfo() {
		if (TransactionAspectSupport.currentTransactionStatus().isNewTransaction()) {
			System.out.println("new transaction");
		} else {
			System.out.println("old transaction");
		}
	}

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public List<Student> getStudent(Integer id) {
		printTransactionInfo();
		return studentRepository.findAll();
	}

	@Transactional//(rollbackFor = Exception.class)
	public Student updateStudent(Student student, Integer id) {
		printTransactionInfo();
		student.setId(id);
		Student student2 = studentRepository.saveAndFlush(student);
				//int c = 2/0;
		return student2;
	}

	public void deleteStudent(Integer id) {
		printTransactionInfo();
		studentRepository.deleteById(id);
	}

}