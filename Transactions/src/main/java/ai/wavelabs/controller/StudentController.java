package ai.wavelabs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ai.wavelabs.modules.Student;
import ai.wavelabs.service.StudentService;

@RestController
public class StudentController 
{
	@Autowired
	private StudentService studentService;
	
	@GetMapping(value="students")
	public ResponseEntity<List<Student>> getAllStu(@PathVariable("id") int id)
	{
		List<Student> li=studentService.getStudent(id);
		return ResponseEntity.status(200).body(li);
	}
	
	@PostMapping(value="students")
	public ResponseEntity<Student> addStu(@RequestBody Student stu)
	{
		Student s=studentService.saveStudent(stu);
		return ResponseEntity.status(201).body(s);
		
	}
	@PutMapping(value="/students/{id}")
	public ResponseEntity<Student> updateStudent(@RequestBody Student stu,@PathVariable("id") int id)
	{
		return ResponseEntity.status(201).body(studentService.updateStudent(stu, id));
		
	}
	 
	
	
	

}
