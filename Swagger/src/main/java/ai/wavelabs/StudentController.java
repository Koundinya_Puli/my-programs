package ai.wavelabs;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController									
public class StudentController {

	@Autowired
	StudentService studentService;

	List<StudentDb> list = new ArrayList<>();

	@GetMapping(value = "test")
	public String sayHello() {
		return "Test";
	}

	@GetMapping(value = "/students", produces = { MediaType.APPLICATION_JSON_VALUE, })
	public ResponseEntity<List<StudentDb>> getStudents(@RequestParam(name = "age", required = false) Integer age) {
		List<StudentDb> students = studentService.getAllStudents();
		return ResponseEntity.status(200).body(students);
	}

	@PostMapping(value = "students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<StudentDb> saveStudent(@RequestBody StudentDb student) {
		StudentDb s = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(s);
	}

	@PutMapping("students/{id}")
	public ResponseEntity<StudentDb> updateStudent(@PathVariable("id") Integer id, @RequestBody StudentDb student) {
		studentService.updateStudent(id, student);
		return ResponseEntity.status(200).body(student);
	}

	@DeleteMapping("students/{id}")
	public void deleteStudent(@PathVariable("id") Integer id) {
	studentService.deleteStudent(id);
	
	}
}