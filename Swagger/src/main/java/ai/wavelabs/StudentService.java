package ai.wavelabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	public StudentDb saveStudent(StudentDb student) {
		return studentRepository.save(student);

	}

	public List<StudentDb> getAllStudents() {
		return studentRepository.findAll();
	}

	public StudentDb updateStudent(Integer id, StudentDb student) {
		student.setId(id);
		return studentRepository.save(student);
	}
	public void deleteStudent(Integer id) {
		studentRepository.deleteById(id);
		}

}