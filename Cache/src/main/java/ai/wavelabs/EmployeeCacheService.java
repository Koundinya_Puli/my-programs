package ai.wavelabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;


@Service
public class EmployeeCacheService {

	@Autowired
	public EmployeeCacheRepository cacherepository;
	
	@Cacheable(cacheNames = "employees")
	public List<EmployeeCache> getAllEmployeeCache() {
		List<EmployeeCache> li = cacherepository.findAll();
		return li;
	}
	
	
	public EmployeeCache saveEmployeeCache(EmployeeCache cachee) {
		return cacherepository.save(cachee);
	}
	private void printTransactionInfo() {
		if (TransactionAspectSupport.currentTransactionStatus().isNewTransaction()) {
			System.out.println("new transaction");
		} else {
			System.out.println("old transaction");
		}
	}

	@Cacheable(cacheNames = "employees")
	public EmployeeCache getEmployeeCache(Integer id) {
		return cacherepository.findById(id).get();
	}

	@CachePut(cacheNames = "employees", key = "#po.empid")
	public EmployeeCache updateEmployeeCache(EmployeeCache cachee, Integer id) {
		cachee.setEmpid(id);
		EmployeeCache employee2 = cacherepository.saveAndFlush(cachee);
		return employee2;
	}

	@CacheEvict(key = "#id", cacheNames = "employees")
	public void deleteEmployeeCache(Integer id) {
		cacherepository.deleteById(id);
	}

	
	
}
