package ai.wavelabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmployeeCacheController {
	
	@Autowired
	public EmployeeCacheService cacheservice;
	
	@GetMapping(value="employeecache")
	public ResponseEntity<List<EmployeeCache>> getAllEmployeeCache() {
		List<EmployeeCache> li = cacheservice.getAllEmployeeCache();
		return ResponseEntity.status(200).body(li);
	}

	@PostMapping(value = "employeecache")
	public ResponseEntity<EmployeeCache> updateEmployeeCache(@RequestBody EmployeeCache cachebody) {
		return ResponseEntity.status(201).body(cacheservice.saveEmployeeCache(cachebody));

	}


}
