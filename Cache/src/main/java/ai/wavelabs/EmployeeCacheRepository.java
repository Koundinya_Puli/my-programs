package ai.wavelabs;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeCacheRepository extends JpaRepository<EmployeeCache,Integer>{

}
