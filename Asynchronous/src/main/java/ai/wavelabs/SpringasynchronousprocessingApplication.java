package ai.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringasynchronousprocessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringasynchronousprocessingApplication.class, args);
	}

}
