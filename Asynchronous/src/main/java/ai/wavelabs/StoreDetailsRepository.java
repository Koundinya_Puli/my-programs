package ai.wavelabs;

import org.springframework.data.jpa.repository.JpaRepository;


public interface StoreDetailsRepository extends JpaRepository<StoreDetails, Integer>
{


}