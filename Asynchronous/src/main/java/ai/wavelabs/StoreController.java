package ai.wavelabs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import com.example.demo.module.StoreDetails;
//import com.example.demo.service.StoreService;

@RestController
public class StoreController {
	@Autowired
	private StoreService storeService;


	@GetMapping(value = "/store")
	public ResponseEntity<List<StoreDetails>> getAllStoreDetails() {
		List<StoreDetails> list = storeService.getAllStoreDetails();
		return ResponseEntity.status(201).body(list);
	}

	@PostMapping(value = "/store")
	public ResponseEntity<StoreDetails> saveStoreDetails(@RequestBody StoreDetails storeDetails) {
		StoreDetails store = storeService.saveStoreDetails(storeDetails);
		return ResponseEntity.status(200).body(store);
	}

	@PutMapping(value = "/store/{gstn}")
	public ResponseEntity<StoreDetails> updateStoreDetails(@RequestBody StoreDetails storeDetails,
			@PathVariable("gstn") String gstn) {
		storeDetails.setGstn(gstn);

		return ResponseEntity.status(200).body(storeService.updateStroreDetails(storeDetails));
	}

}